<?php

namespace Octave\Tests;

use Octave\Bundle\SecurityBundle\Doctrine\UserManager;
use Octave\Bundle\SecurityBundle\Entity\User;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class SecurityTestCase extends DoctrineTestCase
{
	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var UserManager
	 */
	protected $userManager;

	/**
	 * Authenticates as default User
	 */
	protected function loginAsUser()
	{
		$session = $this->getSession();

		$token = new UsernamePasswordToken($this->user, 'password', self::FIREWALL_CONTEXT, ['ROLE_OCTAVE_USER']);
		$session->set('_security_'.self::FIREWALL_CONTEXT, serialize($token));
		$session->save();

		$cookie = new Cookie($session->getName(), $session->getId());
		$this->client->getCookieJar()->set($cookie);
	}

	public function setUp()
	{
		parent::setUp();

		/** @var UserManager */
		$userManager = $this->serviceContainer->get(UserManager::class);
		$this->userManager = $userManager;

		$this->user = $userManager->create('user', 'John', 'Doe', 'john@doe.test', 'password', true);
	}

	/**
	 * @return UserManager
	 */
	protected function getUserManager(): UserManager
	{      
		return $this->userManager;
	}

	/**
	 * @return User
	 */
	protected function getUser(): User
	{
		return $this->user;
	}
}
