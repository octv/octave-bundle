<?php

namespace Octave\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class DoctrineTestCase extends TestCase
{
	/**
	 * @var ?EntityManagerInterface
	 */
	protected $entityManager;

	public function setUp()
	{
		parent::setUp();

		/** @var RegistryInterface */
		$doctrine = $this->serviceContainer->get('doctrine');
	
		/** @var EntityManagerInterface */
		$entityManager = $doctrine->getManager();
		$this->entityManager = $entityManager;

		$this->runCommand('doctrine:database:drop --force');
		$this->runCommand('doctrine:database:create');
		$this->runCommand('doctrine:schema:create');
	}

	protected function tearDown()
	{
		$this->runCommand('doctrine:database:drop --force');

		parent::tearDown();

		$this->getEntityManager()->close();
		$this->entityManager = null;
	}

	/**
	 * @return EntityManagerInterface
	 */
	protected function getEntityManager(): EntityManagerInterface
	{   
		if (!$this->entityManager) {
			throw new \Exception('The entity manager has already been closed');
		}

		return $this->entityManager;
	}
}
