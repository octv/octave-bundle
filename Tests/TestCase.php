<?php

namespace Octave\Tests;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class TestCase extends WebTestCase
{
	const FIREWALL_CONTEXT = 'octave_admin';

	/**
	 * @var Client
	 */
	protected $client;

	/**
	 * @var ContainerInterface
	 */
	protected $serviceContainer;

	/**
	 * @var Application
	 */
	protected $application;

	public function setUp()
	{
		$this->client = static::createClient();

		/** @var ContainerInterface */
		$container = $this->client->getContainer();
		$this->serviceContainer = $container;

		$this->application = new Application($this->client->getKernel());
		$this->application->setAutoExit(false);

		parent::setUp();
	}

	protected static function getKernelClass()
	{
		return TestKernel::class;
	}

	protected function runCommand($command)
	{
		$command = sprintf('%s --quiet', $command);

		return $this->application->run(new StringInput($command));
	}

	/**
	 * Fakes authentication as default User
	 * NOTE: If you want to use an actual user, use Octave\Tests\SecurityTestCase instead
	 */
	protected function loginAsUser()
	{
		$session = $this->getSession();
		$firewallName = self::FIREWALL_CONTEXT;
		$firewallContext = self::FIREWALL_CONTEXT;

		$token = new UsernamePasswordToken('user', 'password', $firewallName, ['ROLE_OCTAVE_USER']);
		$session->set('_security_'.$firewallContext, serialize($token));
		$session->save();

		$cookie = new Cookie($session->getName(), $session->getId());
		$this->client->getCookieJar()->set($cookie);
	}

	/**
	 * Checks if the client is logged in
	 *
	 * @return bool
	 */
	protected function isLoggedIn()
	{
		return $this->getSession()->get('_security_'.self::FIREWALL_CONTEXT) != null;
	}

	/**
	 * @return Client
	 */
	protected function getClient(): Client
	{
		return $this->client;
	}

	/**
	 * @return ContainerInterface
	 */
	protected function getServiceContainer(): ContainerInterface
	{      
		return $this->serviceContainer;
	}

	/**
	 * @return Application
	 */
	protected function getApplication(): Application
	{
		return $this->application;
	}

	/**
	 * @return SessionInterface     
	 */
	protected function getSession(): SessionInterface
	{
		/** @var SessionInterface */
		$session = $this->serviceContainer->get('session');

		return $session;
	}
}
