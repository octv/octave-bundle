<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Tests;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

/**
 * Test kernel used for functional tests
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class TestKernel extends Kernel
{
	use MicroKernelTrait;
	
	const CONFIG_EXTS = '.{php,xml,yaml,yml}';

	public function registerBundles()
	{
		return [
			new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
			new \Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle(),
			new \Octave\Bundle\CoreBundle\OctaveCoreBundle(),
			new \Octave\Bundle\MenuBundle\OctaveMenuBundle(),
			new \Octave\Bundle\SecurityBundle\OctaveSecurityBundle(),
			new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
			new \Symfony\Bundle\SecurityBundle\SecurityBundle(),
			new \Symfony\Bundle\TwigBundle\TwigBundle(),
			new \Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle()
		];
	}

	protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
	{
		$confDir = __DIR__.'/Resources/config';

		$loader->load($confDir.'/{packages}/*'.self::CONFIG_EXTS, 'glob');
		$loader->load($confDir.'/{services}'.self::CONFIG_EXTS, 'glob');

		$confDir = $this->getProjectDir() . '/Bundle/*/Tests/Resources/config';

		$loader->load($confDir.'/{packages}/*'.self::CONFIG_EXTS, 'glob');
		$loader->load($confDir.'/{services}'.self::CONFIG_EXTS, 'glob');
	}

	protected function configureRoutes(RouteCollectionBuilder $routes)
	{
		$confDir = __DIR__.'/Resources/config';

		$routes->import($confDir.'/{routes}'.self::CONFIG_EXTS, '/', 'glob');

		$confDir = $this->getProjectDir() . '/Bundle/*/Tests/Resources/config';

		$routes->import($confDir.'/{routes}'.self::CONFIG_EXTS, '/', 'glob');
	}

}