# Octave CMS / Octave Bundle

Check the README files in the seperate bundles for more information:

## Included bundles in this development project 

* [Core](https://gitlab.com/octv/octave-bundle/blob/master/Bundle/CoreBundle/README.md)
* [Security](https://gitlab.com/octv/octave-bundle/blob/master/Bundle/SecurityBundle/README.md)

## Bundles that already have been seperated from this project into their own project

* [Menu](https://gitlab.com/octv/menu-bundle/blob/master/README.md)