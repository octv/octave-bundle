<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Octave\Bundle\SecurityBundle\Entity\User;
use Octave\Bundle\SecurityBundle\Exception\UsernameNotUniqueException;
use Octave\Bundle\SecurityBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Manager for the security user entity
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class UserManager implements UserProviderInterface
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * @var EntityManagerInterface
	 */
	private $entityManager;

	/**
	 * @var UserPasswordEncoderInterface
	 */
	private $passwordEncoder;

	/**
	 * UserManager constructor
	 *
	 * @param UserRepository 				$userRepository
	 * @param EntityManagerInterface 		$entityManager
	 * @param UserPasswordEncoderInterface	$passwordEncoder
	 */
	public function __construct(
		UserRepository $userRepository, 
		EntityManagerInterface $entityManager, 
		UserPasswordEncoderInterface $passwordEncoder
	) {
		$this->userRepository = $userRepository;
		$this->entityManager = $entityManager;
		$this->passwordEncoder = $passwordEncoder;
	}

	/**
	 * Creates an User entity for the given username if there is no other user with this username
	 * 
	 * @param string $username
	 *
	 * @throws UsernameNotUniqueException
	 * 
	 * @return User
	 */
	public function createForUsername(string $username): User
	{
		try {
			$this->loadUserByUsername($username);
		} catch (UsernameNotFoundException $e) {
			// No User found for the given username, create a new User
			$user = new User();
			$user->setUsername($username);

			return $user;
		}

		// User already exists, throw an UsernameNotUniqueException
		throw new UsernameNotUniqueException($username);
	}

	/**
	 * Encodes and updates the given password for the given user
	 *
	 * @param User 		$user
	 * @param string 	$password
	 *
	 * @return User
	 */
	public function updatePassword(User $user, string $password): User
	{
		// Encode the password
		$encodedPassword = $this->passwordEncoder->encodePassword($user, $password);

		// Update the password
		$user->setPassword($encodedPassword);

		return $user;
	}

	/**
	 * Creates an User entity for the given details
	 * 
	 * @param string 	$username
	 * @param string 	$name
	 * @param string 	$surname
	 * @param string 	$emailAddress
	 * @param string 	$password
	 * @param bool 		$isActive
	 * @param bool 	 	$persistUser	If set to true, the user will be persisted and flushed in the entity manager
	 *
	 * @throws UsernameNotUniqueException
	 * 
	 * @return User
	 */
	public function create(string $username, string $name, string $surname, string $emailAddress, string $password, bool $isActive = false, bool $persistUser = true): User
	{
		// Create the user for the username
		$user = $this->createForUsername($username);

		// Fill the user entity with the given details
		$user->setName($name);
		$user->setSurname($surname);
		$user->setEmailAddress($emailAddress);
		$user->setIsActive($isActive);

		$this->updatePassword($user, $password);

		if ($persistUser) {
			$this->entityManager->persist($user);
			$this->entityManager->flush();
		}

		return $user;
	}

	/**
	 * Updates the logged in User's lastLoggedInAt property to the current DateTime
	 * 
	 * @param  User $user
	 * @param  bool $persistUser
	 * 
	 * @return User
	 */
	public function updateLastLoginAt(User $user, bool $persistUser = true): User
	{
		// Update last login
		$user->setLastLoginAt(new \DateTime());

		if ($persistUser) {
			// Persist the data to database.
			$this->entityManager->persist($user);
			$this->entityManager->flush();
		}

		return $user;
	}

	/**
	 * {@inheritdoc}
	 */
	public function loadUserByUsername($username)
	{
		// Find an user for the given username
		$user = $this->userRepository->findOneBy(['username' => $username]);

		if (!$user) {
			// No user found, throw an UsernameNotFoundException
			$message = sprintf(
				'Unable to find an Octave\Bundle\SecurityBundle\Entity\User object identified by "%s".',
				$username
			);
			throw new UsernameNotFoundException($message);
		}

		return $user;
	}

	/**
	 * Refreshes the user
	 *
	 * @param  UserInterface $user
	 *
	 * @throws UnsupportedUserException
	 *
	 * @return User
	 */
	public function refreshUser(UserInterface $user)
	{
		if (!$user instanceof User) {
			throw new UnsupportedUserException(
				sprintf(
					'Instances of "%s" are not supported.',
					get_class($user)
				)
			);
		}

		if (!$user->getId()) {
			// The User has not been persisted yet, return the original entity
			return $user;
		}

		/** @var User */
		$user = $this->userRepository->find($user->getId());

		$this->entityManager->refresh($user);

		return $user;
	}

	/**
	 * {@inheritdoc}
	 */
	public function supportsClass($class)
	{
		return User::class === $class;
	}
}