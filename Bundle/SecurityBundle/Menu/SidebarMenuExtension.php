<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Menu;

use Octave\Bundle\MenuBundle\Builder\Extension\ExtensionInterface;
use Octave\Bundle\MenuBundle\Model\MenuInterface;

/**
 * Sidebar menu builder
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class SidebarMenuExtension implements ExtensionInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function extend(MenuInterface $menu)
	{
		$menu->addChild('octave_security');
	}
}