<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Tests\Unit\Menu;

use Octave\Bundle\MenuBundle\Model\Menu;
use Octave\Bundle\SecurityBundle\Menu\SidebarMenuExtension;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\SecurityBundle\Menu\SidebarMenuExtension
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class SidebarMenuExtensionTest extends WebTestCase
{
	/**
	 * Test if the menu builder can be built
	 */
	public function testBuild()
	{
		$extension = new SidebarMenuExtension();
		$menu = new Menu();

		$this->assertFalse($menu->hasChild('octave_security'));

		$extension->extend($menu);

		$this->assertTrue($menu->hasChild('octave_security'));
	}
}