<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Tests\Unit\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Octave\Bundle\SecurityBundle\Doctrine\UserManager;
use Octave\Bundle\SecurityBundle\Entity\User;
use Octave\Bundle\SecurityBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * Unit tests for Octave\Bundle\SecurityBundle\Doctrine\UserManager
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class UserManagerTest extends WebTestCase
{
	/** 
	 * Test if updating a User's lastLoginAt property works
	 */
	public function testUpdateLastLoginAt()
	{
		$userManager = $this->getUserManager();
		$user = new User();

		// Get the current lastLoginAt for comparing later
		$oldLastLoginAt = $user->getLastLoginAt();

		$userManager->updateLastLoginAt($user);

		$this->assertTrue($user->getLastLoginAt() > $oldLastLoginAt);
	}

	/**
	 * Test if supportsClass function works properly
	 */
	public function testSupportsClass()
	{
		$userManager = $this->getUserManager();

		$this->assertTrue($userManager->supportsClass(User::class));
		$this->assertFalse($userManager->supportsClass(UserInterface::class));
	}

	/**
	 * @return UserManager
	 */
	private function getUserManager(): UserManager
	{
		$userRepository = $this->createMock(UserRepository::class);
		$entityManager = $this->createMock(EntityManagerInterface::class);
		$passwordEncoder = $this->createMock(UserPasswordEncoderInterface::class);

		$userManager = new UserManager($userRepository, $entityManager, $passwordEncoder);

		return $userManager;
	}
}