<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Tests\Unit\Entity;

use Octave\Bundle\SecurityBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\SecurityBundle\Entity\User
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class UserTest extends WebTestCase
{
	/**
	 * Test if the id works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getId
	 */
	public function testId()
	{
		$user = new User();

		$this->assertNull($user->getId());
	}

	/**
	 * Test if the username works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getUsername
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::setUsername
	 */
	public function testUsername()
	{
		$user = new User();

		$this->assertNull($user->getUsername());
		$user->setUsername('john@doe.test');
		$this->assertSame('john@doe.test', $user->getUsername());
	}

	/**
	 * Test if the name works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getName
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::setName
	 */
	public function testName()
	{
		$user = new User();

		$this->assertNull($user->getName());
		$user->setName('John');
		$this->assertSame('John', $user->getName());
	}

	/**
	 * Test if the surname works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getSurname
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::setSurname
	 */
	public function testSurname()
	{
		$user = new User();

		$this->assertNull($user->getSurname());
		$user->setSurname('Doe');
		$this->assertSame('Doe', $user->getSurname());
	}

	/**
	 * Test if the email address works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getEmailAddress
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::setEmailAddress
	 */
	public function testEmailAddress()
	{
		$user = new User();

		$this->assertNull($user->getEmailAddress());
		$user->setEmailAddress('john@doe.test');
		$this->assertSame('john@doe.test', $user->getEmailAddress());
	}

	/**
	 * Test if the password works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getPassword
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::setPassword
	 */
	public function testPassword()
	{
		$user = new User();

		$this->assertNull($user->getPassword());
		$user->setPassword('sunshine');
		$this->assertSame('sunshine', $user->getPassword());
	}

	/**
	 * Test if the plain password and erasing credentials works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getPlainPassword
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::setPlainPassword
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::eraseCredentials
	 */
	public function testPlainPassword()
	{
		$user = new User();

		$this->assertNull($user->getPlainPassword());
		$user->setPlainPassword('sunshine');
		$this->assertSame('sunshine', $user->getPlainPassword());

		// Test if it gets cleared properly by the eraseCredentials method
		$user->eraseCredentials();
		$this->assertNull($user->getPlainPassword());
	}

	/**
	 * Test if the active property and isEnabled method works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::isActive
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::setIsActive
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::isEnabled
	 */
	public function testIsActive()
	{
		$user = new User();

		$this->assertFalse($user->isActive());
		$this->assertFalse($user->isEnabled());
		$user->setIsActive(true);
		$this->assertTrue($user->isActive());
		$this->assertTrue($user->isEnabled());
	}

	/**
	 * Test if the last login works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getLastLoginAt
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::setLastLoginAt
	 */
	public function testLastLoginAt()
	{
		$user = new User();

		$this->assertNull($user->getLastLoginAt());
		$user->setLastLoginAt(new \DateTime('1995-06-08 11:11'));
		$lastLoginAt = $user->getLastLoginAt();
		$this->assertTrue($lastLoginAt != null);
		$this->assertSame('1995-06-08 11:11', $lastLoginAt->format('Y-m-d H:i'));
	}

	/**
	 * Test if the roles work properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getRoles
	 */
	public function testRoles()
	{
		$user = new User();

		$this->assertSame(1, count($user->getRoles()));
		$this->assertSame('ROLE_OCTAVE_USER', $user->getRoles()[0]);
	}

	/**
	 * Test if the salt works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::getSalt
	 */
	public function testSalt()
	{
		$user = new User();

		$this->assertNull($user->getSalt());
	}

	/**
	 * Test if the account non expired works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::isAccountNonExpired
	 */
	public function testIsAccountNonExpired()
	{
		$user = new User();

		$this->assertTrue($user->isAccountNonExpired());
	}

	/**
	 * Test if the account non locked works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::isAccountNonLocked
	 */
	public function testIsAccountNonLocked()
	{
		$user = new User();

		$this->assertTrue($user->isAccountNonLocked());
	}

	/**
	 * Test if the credentials non expired works properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\Entity\User::isCredentialsNonExpired
	 */
	public function testIsCredentialsNonExpired()
	{
		$user = new User();

		$this->assertTrue($user->isCredentialsNonExpired());
	}
}