<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Tests\Unit\DependencyInjection;

use Octave\Bundle\SecurityBundle\DependencyInjection\OctaveSecurityExtension;
use Octave\Bundle\SecurityBundle\Doctrine\UserManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Unit tests for Octave\Bundle\SecurityBundle\DependencyInjection\OctaveSecurityExtension
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class OctaveSecurityExtensionTest extends WebTestCase
{
	/**
	 * Test if the extension loads the configuration properly
	 *
	 * @covers Octave\Bundle\SecurityBundle\DependencyInjection\OctaveSecurityExtension::load
	 */
	public function testLoad()
	{
		$extension = new OctaveSecurityExtension();
		$container = new ContainerBuilder();
		$extension->load([], $container);

		// Test one of the services
		$this->assertTrue($container->hasDefinition(UserManager::class));

		// Test one of aliases
		$this->assertTrue($container->hasAlias('octave.user_provider'));
	}
}