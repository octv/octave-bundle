<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Tests\Unit\Command;

use Octave\Bundle\SecurityBundle\Command\CreateUserCommand;
use Octave\Bundle\SecurityBundle\Doctrine\UserManager;
use Octave\Bundle\SecurityBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Unit tests for Octave\Bundle\SecurityBundle\Command\CreateUserCommand
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class CreateUserCommandTest extends WebTestCase
{	
	/**
	 * Create a non-interactive command tester for the CreateUserCommand
	 * 
	 * @return CommandTester
	 */
	private function createCommandTester()
	{
		$userManager = $this->createMock(UserManager::class);
		$command = new CreateUserCommand($userManager);
		$commandTester = new CommandTester($command);

		return $commandTester;
	}

	/**
	 * Executes the CreateUserCommand for the given parameters
	 *
	 * @param CommandTester $commandTester
	 * @param string 		$username
	 * @param string 		$name
	 * @param string 		$surname
	 * @param string 		$emailAddress
	 * @param string 		$password
	 *
	 * @throws InvalidOptionException
	 * 
	 * @return int
	 */
	private function execute(
		CommandTester $commandTester, 
		string $username, 
		string $name, 
		string $surname, 
		string $emailAddress, 
		string $password
	) {
		return $commandTester->execute([
			'username' => $username,
			'name' => $name,
			'surname' => $surname,
			'emailAddress' => $emailAddress,
			'password' => $password,
		], [
			'decorated' => false,
			'interactive' => false,
		]);
	}

	/**
	 * Tests if executing the command with an invalid username throws an InvalidOptionException
	 */
	public function testExecuteWithoutUsername()
	{
		$this->expectException(InvalidOptionException::class);

		$this->execute($this->createCommandTester(), '', 'name', 'surname', 'email@address.test', 'password');
	}

	/**
	 * Tests if executing the command with an invalid name throws an InvalidOptionException
	 */
	public function testExecuteWithoutName()
	{
		$this->expectException(InvalidOptionException::class);

		$this->execute($this->createCommandTester(), 'username', '', 'surname', 'email@address.test', 'password');
	}

	/**
	 * Tests if executing the command with an invalid surname throws an InvalidOptionException
	 */
	public function testExecuteWithoutSurname()
	{
		$this->expectException(InvalidOptionException::class);

		$this->execute($this->createCommandTester(), 'username', 'name', '', 'email@address.test', 'password');
	}

	/**
	 * Tests if executing the command with an invalid email address throws an InvalidOptionException
	 */
	public function testExecuteWithoutEmailAddress()
	{
		$this->expectException(InvalidOptionException::class);

		$this->execute($this->createCommandTester(), 'username', 'name', 'surname', '', 'password');
	}

	/**
	 * Tests if executing the command with an invalid password throws an InvalidOptionException
	 */
	public function testExecuteWithoutPassword()
	{
		$this->expectException(InvalidOptionException::class);

		$this->execute($this->createCommandTester(), 'username', 'name', 'surname', 'email@address.test', '');
	}

	/**
	 * Tests if executing the command with valid data works properly
	 */
	public function testExecuteWithValidData()
	{
		$userManager = $this->createMock(UserManager::class);
		$command = new CreateUserCommand($userManager);
		$commandTester = new CommandTester($command);

		$mockUser = new User();
		$mockUser->setUsername('username');
		$userManager
			->expects($this->once())
			->method('create')
			->willReturn($mockUser)
		;

		$exitCode = $this->execute($commandTester, 'username', 'name', 'surname', 'email@address.test', 'password');

		$this->assertTrue(strpos($commandTester->getDisplay(), 'Created user username') !== false);
		$this->assertEquals($exitCode, 0);
	}

	/**
	 * Creates an interactive command tester
	 *
	 * @param string 	$username
	 * @param string 	$name
	 * @param string 	$surname
	 * @param string 	$emailAddress
	 * @param string 	$password
	 * @param bool 		$isValidData	Determines if the given data should be considered valid
	 * 
	 * @return CommandTester
	 */
	private function createInteractiveCommandTester(
		string 	$username, 
		string 	$name, 
		string 	$surname, 
		string 	$emailAddress, 
		string 	$password,
		bool 	$isValidData = false
	) {
		$helper = new QuestionHelper();
		$helper::disableStty();

		$userManager = $this->createMock(UserManager::class);
		$command = new CreateUserCommand($userManager);
		$command->setHelperSet(new HelperSet(array(new FormatterHelper(), $helper)));
		$commandTester = new CommandTester($command);

		$commandTester->setInputs([$username, $name, $surname, $emailAddress, $password]);

		if ($isValidData) {
			$mockUser = new User();
			$mockUser->setUsername($username);
			$userManager
				->expects($this->once())
				->method('createForUsername')
				->willReturn($mockUser)
			;
			$userManager
				->expects($this->once())
				->method('create')
				->willReturn($mockUser)
			;
		}

		return $commandTester;
	}

	/**
	 * Tests if interacting with an invalid username throws a RuntimeException
	 */
	public function testInteractWithoutUsername()
	{
		$this->expectException(RuntimeException::class);

		$commandTester = $this->createInteractiveCommandTester('', 'name', 'surname', 'email@address.test', 'password');
		$commandTester->execute([], ['decorated' => false, 'interactive' => true]);
	}

	/**
	 * Tests if interacting with an invalid name throws a RuntimeException
	 */
	public function testInteractWithoutName()
	{
		$this->expectException(RuntimeException::class);

		$commandTester = $this->createInteractiveCommandTester('username', '', 'surname', 'email@address.test', 'password');
		$commandTester->execute([], ['decorated' => false, 'interactive' => true]);
	}

	/**
	 * Tests if interacting with an invalid username throws a RuntimeException
	 */
	public function testInteractWithoutSurname()
	{
		$this->expectException(RuntimeException::class);

		$commandTester = $this->createInteractiveCommandTester('username', 'name', '', 'email@address.test', 'password');
		$commandTester->execute([], ['decorated' => false, 'interactive' => true]);
	}

	/**
	 * Tests if interacting with an invalid email address throws a RuntimeException
	 */
	public function testInteractWithoutEmailAddress()
	{
		$this->expectException(RuntimeException::class);

		$commandTester = $this->createInteractiveCommandTester('username', 'name', 'surname', '', 'password');
		$commandTester->execute([], ['decorated' => false, 'interactive' => true]);
	}

	/**
	 * Tests if interacting with an invalid password throws a RuntimeException
	 */
	public function testInteractWithoutPassword()
	{
		$this->expectException(RuntimeException::class);

		$commandTester = $this->createInteractiveCommandTester('username', 'name', 'surname', 'email@address.test', '');
		$commandTester->execute([], ['decorated' => false, 'interactive' => true]);
	}

	/**
	 * Tests if interacting with valid data works properly
	 */
	public function testInteractWithValidData()
	{
		$commandTester = $this->createInteractiveCommandTester('username', 'name', 'surname', 'email@address.test', 'password', true);
		$exitCode = $commandTester->execute([], ['decorated' => false, 'interactive' => true]);

		$this->assertTrue(strpos($commandTester->getDisplay(), 'Created user username') !== false);
		$this->assertEquals($exitCode, 0);
	}
}