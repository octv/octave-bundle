<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Tests\Functional\Controller;

use Octave\Bundle\SecurityBundle\Controller\SecurityController;
use Octave\Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Functional tests for Octave\Bundle\SecurityBundle\Controller\SecurityController
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class SecurityControllerTest extends TestCase
{
	/**
	 * Test if the login page is reachable
	 *
	 * @covers Octave\Bundle\SecurityBundle\Controller\SecurityController::login
	 */
	public function testLogin()
	{
		$client = $this->getClient();
		$client->request('GET', '/octave-admin/login');
		$this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
	}

	/**
	 * Test if the logout is functional
	 *
	 * @covers Octave\Bundle\SecurityBundle\Controller\SecurityController::logout
	 */
	public function testLogout()
	{
		$client = $this->getClient();
		$this->loginAsUser();

		// At this stage the user should still be logged in
		$this->assertTrue($this->isLoggedIn());

		$client->request('GET', '/octave-admin/logout');
		$this->assertTrue($client->getResponse()->isRedirection());

		// At this stage the user should be logged out
		$this->assertFalse($this->isLoggedIn());

		// Test the logout exception if the function is actually called
		$securityController = $this->getMockForAbstractClass(SecurityController::class);

		$this->expectException(\Exception::class);
		$securityController->logout();
	}
}