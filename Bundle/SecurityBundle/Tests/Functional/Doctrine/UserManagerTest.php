<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Tests\Functional\Doctrine;

use Octave\Bundle\SecurityBundle\Doctrine\UserManager;
use Octave\Bundle\SecurityBundle\Entity\User;
use Octave\Bundle\SecurityBundle\Exception\UsernameNotUniqueException;
use Octave\Tests\SecurityTestCase;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * Functional tests for Octave\Bundle\SecurityBundle\Doctrine\UserManager
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class UserManagerTest extends SecurityTestCase
{
	/**
	 * Test if creating an User fully works
	 */
	public function testCreate()
	{
		$userManager = $this->getUserManager();

		// Create an user
		$user = $userManager->create(
			'username',
			'name',
			'surname',
			'email@address.test',
			'password',
			true, // Active
			true  // Persist the user
		);

		$this->assertEquals($user->getUsername(), 'username');
		$this->assertEquals($user->getName(), 'name');
		$this->assertEquals($user->getSurname(), 'surname');
		$this->assertEquals($user->getEmailAddress(), 'email@address.test');
		$password = $user->getPassword();		
		$this->assertTrue($password != null);
		$this->assertTrue(strlen($password) > 0);
		$this->assertTrue($user->isActive());

		// Try to create another user for the same username, it should throw an UsernameNotUniqueException
		$this->expectException(UsernameNotUniqueException::class);
		$userManager->createForUsername('username');
	}

	/**
	 * Test if creating an User by username works
	 */
	public function testCreateForUsername()
	{
		$userManager = $this->getUserManager();

		$user = $userManager->createForUsername('username');

		$this->assertEquals($user->getUsername(), 'username');
	}

	/**
	 * Test if updating an User password works
	 */
	public function testUpdatePassword()
	{
		$userManager = $this->getUserManager();

		$user = $userManager->createForUsername('username');
		$user->setPassword('old-password');

		$userManager->updatePassword($user, 'new-password');

		$this->assertTrue($user->getPassword() != 'old-password');
		$this->assertTrue($user->getPassword() != 'new-password');
	}

	/**
	 * Test if loading an User by an username works
	 */
	public function testLoadUserByUsername()
	{
		$userManager = $this->getUserManager();
		$username = $this->getUser()->getUsername();

		$this->assertNotNull($username);
		$this->assertEquals($userManager->loadUserByUsername($username), $this->getUser());

		$this->expectException(UsernameNotFoundException::class);
		$userManager->loadUserByUsername('non_existing_username');
	}

	/**
	 * Test if refreshing an User works
	 */
	public function testRefreshUser()
	{
		$userManager = $this->getUserManager();

		$user = $this->getUser();
		$user->setName('altered_name');

		$user = $userManager->refreshUser($user);

		$this->assertTrue($user->getName() != 'altered_name');

		// Try with a new user
		$user = $userManager->createForUsername('username');
		$user = $userManager->refreshUser($user);
		$this->assertEquals($user->getId(), null);
		$this->assertEquals($user->getUsername(), 'username');

		// Test with a different User class
		$user = new \Symfony\Component\Security\Core\User\User('username', 'password');
		$this->expectException(UnsupportedUserException::class);
		$userManager->refreshUser($user);
	}
}