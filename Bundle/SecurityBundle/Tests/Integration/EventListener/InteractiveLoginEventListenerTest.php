<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Tests\Integration\EventListener;

use Octave\Bundle\SecurityBundle\Doctrine\UserManager;
use Octave\Bundle\SecurityBundle\Entity\User;
use Octave\Bundle\SecurityBundle\EventListener\InteractiveLoginEventListener;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Integration tests for Octave\Bundle\SecurityBundle\EventListener\InteractiveLoginEventListener
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class InteractiveLoginEventListenerTest extends WebTestCase
{
	/**
	 * Test if the event listener works properly
	 */
	public function testOnSecurityInteractiveLogin()
	{
		$user = new User();
		$user->setLastLoginAt(new \DateTime('-1 day'));

		$request = $this->createMock(Request::class);
		$token = new UsernamePasswordToken($user, 'password', 'providerKey');
		$userManager = $this->createMock(UserManager::class);
		$userManager
			->expects($this->once())
			->method('updateLastLoginAt')
			->with(
				$this->callback(function(User $user) {
					$user->setLastLoginAt(new \DateTime());
					return true;
				})
			);
		;

		// Manually create the event
		$event = new InteractiveLoginEvent($request, $token);

		$oldLastLoginAt = $user->getLastLoginAt();

		$listener = new InteractiveLoginEventListener($userManager);
		$listener->onSecurityInteractiveLogin($event);

		$this->assertTrue($user->getLastLoginAt() > $oldLastLoginAt);
	}

	/**
	 * Test if the event listener skips the listener without issues when giving a non-Octave User
	 */
	public function testSkipOnSecurityInteractiveLogin()
	{
		$user = new \Symfony\Component\Security\Core\User\User('username', 'password');

		$request = $this->createMock(Request::class);
		$token = new UsernamePasswordToken($user, 'password', 'other_context');
		$userManager = $this->createMock(UserManager::class);

		// Manually create the event
		$event = new InteractiveLoginEvent($request, $token);

		$listener = new InteractiveLoginEventListener($userManager);

		$this->assertEquals($listener->onSecurityInteractiveLogin($event), null);
	}
}