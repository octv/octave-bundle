<?php
/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Command;

use Octave\Bundle\SecurityBundle\Doctrine\UserManager;
use Octave\Bundle\SecurityBundle\Exception\UsernameNotUniqueException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Command for creating a User entity
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class CreateUserCommand extends Command
{
	/**
	 * @var string
	 */
	protected static $defaultName = 'octave:create-user';

	/**
	 * @var UserManager
	 */
	private $userManager;

	/**
	 * CreateUserCommand constructor
	 *
	 * @param UserManager $userManager
	 */
	public function __construct(UserManager $userManager)
	{
		parent::__construct(self::$defaultName);

		$this->userManager = $userManager;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName(self::$defaultName)
			->setDescription('Creates a new Octave user.')
			->setHelp('This command allows you to create a new Octave user.')
			->setDefinition([
				new InputArgument('username', InputArgument::REQUIRED, 'The username'),
				new InputArgument('name', InputArgument::REQUIRED, 'The name'),
				new InputArgument('surname', InputArgument::REQUIRED, 'The surname'),
				new InputArgument('emailAddress', InputArgument::REQUIRED, 'The e-mail address'),
				new InputArgument('password', InputArgument::REQUIRED, 'The password')
			])
		;
	}

	/**
	 * {@inheritdoc}
	 * 
	 * @throws InvalidOptionException
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$username = $input->getArgument('username');
		$name = $input->getArgument('name');
		$surname = $input->getArgument('surname');
		$emailAddress = $input->getArgument('emailAddress');
		$password = $input->getArgument('password');

		// Validate input in case the arguments are given via the command
		if (empty($username) || !is_string($username)) {
			throw new InvalidOptionException('Username must be a non-empty string');
		}
		if (empty($name) || !is_string($name)) {
			throw new InvalidOptionException('Name must be a non-empty string');
		}
		if (empty($surname) || !is_string($surname)) {
			throw new InvalidOptionException('Surname must be a non-empty string');
		}
		if (empty($emailAddress) || !is_string($emailAddress)) {
			throw new InvalidOptionException('E-mail address must be a non-empty string');
		}
		if (empty($password) || !is_string($password)) {
			throw new InvalidOptionException('Password must be a non-empty string');
		}

		// Create and activate the user
		$user = $this->userManager->create($username, $name, $surname, $emailAddress, $password, true);

		$output->writeln(sprintf('Created user <comment>%s</comment>', $user->getUsername()));
	}

	/**
	 * {@inheritdoc}
	 * 
	 * @throws InvalidOptionException 
	 */
	protected function interact(InputInterface $input, OutputInterface $output)
	{
		// Add questions for the arguments that are not set yet
		$questions = [];

		if (!$input->getArgument('username')) {
			$question = new Question('<fg=green>Enter an unique username:</> ');
			$question->setValidator(function ($username) {
				// Make sure the username is not empty
				if (empty($username) || !is_string($username)) {
					throw new InvalidOptionException('Username must be a non-empty string');
				}

				// Try to create an User for this username
				$user = $this->userManager->createForUsername($username);

				return $user->getUsername();
			});

			// Successfully entered the username, add it to the questions array
			$questions['username'] = $question;
		}

		if (!$input->getArgument('name')) {
			$question = new Question('<fg=green>Enter a name:</> ');
			$question->setValidator(function ($name) {
				// Make sure the name is not empty
				if (empty($name) || !is_string($name)) {
					throw new InvalidOptionException('Name must be a non-empty string');
				}
				return $name;
			});

			// Successfully entered the name, add it to the questions array
			$questions['name'] = $question;
		}
		if (!$input->getArgument('surname')) {
			$question = new Question('<fg=green>Enter a surname:</> ');
			$question->setValidator(function ($surname) {
				// Make sure the surname is not empty
				if (empty($surname) || !is_string($surname)) {
					throw new InvalidOptionException('Surname must be a non-empty string');
				}
				return $surname;
			});

			// Successfully entered the surname, add it to the questions array
			$questions['surname'] = $question;
		}
		if (!$input->getArgument('emailAddress')) {
			$question = new Question('<fg=green>Enter a e-mail address:</> ');
			$question->setValidator(function ($emailAddress) {
				// Make sure the email address is not empty
				if (empty($emailAddress) || !is_string($emailAddress)) {
					throw new InvalidOptionException('E-mail address must be a non-empty string');
				}
				return $emailAddress;
			});

			// Successfully entered the email, add it to the questions array
			$questions['emailAddress'] = $question;
		}

		if (!$input->getArgument('password')) {
			$question = new Question('<fg=green>Enter a password:</> ');
			$question->setValidator(function ($password) {
				// Make sure the password is not empty
				if (empty($password) || !is_string($password)) {
					throw new InvalidOptionException('Password must be a non-empty string');
				}
				return $password;
			});

			// Successfully entered the password, hide it and add it to the questions array
			$question->setHidden($output->isDecorated()); // Only hide the question if output is decorated in favor of tests
			$questions['password'] = $question;
		}

		// Fill the arguments with the answered questions
		foreach ($questions as $name => $question) {
			$answer = $this->getHelper('question')->ask($input, $output, $question);
			$input->setArgument($name, $answer);
		}
	}
}