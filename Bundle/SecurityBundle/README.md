# Octave CMS / Security Bundle

## Config

Routing in `config/routes.yaml`:
```yaml
octave_security_bundle:
	resource: 	'@OctaveSecurityBundle/Controller/'
	type:     	annotation
	prefix:   	'%octave_route_prefix%'
```

Security in `config/packages/security.yaml`:
```yaml
security:
	encoders:
		Octave\Bundle\SecurityBundle\Entity\User: bcrypt

	providers:
		octave_admin:
			id: octave.user_provider

	firewalls:
		octave_admin:
			pattern: '%env(OCTAVE_ROUTE_PREFIX)%'
			anonymous: true
			form_login:
				check_path: octave_security_login
				login_path: octave_security_login
				default_target_path: octave_dashboard_index

			provider: octave_admin

			# Allow HTTP Basic authentication (for testing)
			http_basic: ~

			logout:
				path: octave_security_logout
				target: octave_security_login

			remember_me:
				secret:   '%kernel.secret%'
				lifetime: 1814400 # 3 weeks in seconds
				path:     '%env(OCTAVE_ROUTE_PREFIX)%'

	access_control:
		- { path: '%env(OCTAVE_ROUTE_PREFIX)%/login', roles: IS_AUTHENTICATED_ANONYMOUSLY }
		- { path: '%env(OCTAVE_ROUTE_PREFIX)%', roles: ROLE_OCTAVE_USER }

```