<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Security user
 *
 * @author Stan Jansen <stan@stanjan.nl>
 *
 * @ORM\Entity(repositoryClass="Octave\Bundle\SecurityBundle\Repository\UserRepository")
 * @UniqueEntity("username")
 */
class User implements AdvancedUserInterface
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string", length=50, unique=true)
	 * @Assert\NotBlank()
	 * @Assert\Length(
	 *      max = 50
	 * )
	 */
	protected $username;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Length(
	 *      max = 255
	 * )
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Length(
	 *      max = 255
	 * )
	 */
	protected $surname;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 * @Assert\Length(
	 *      max = 255
	 * )
	 */
	protected $emailAddress;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $password;

	/**
	 * Not mapped, used for the encoded password
	 *
	 * @Assert\NotBlank()
	 * @Assert\Length(
	 *      max = 50
	 * )
	 * 
	 * @var ?string
	 */
	protected $plainPassword;

	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $isActive = false;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $lastLoginAt;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getUsername(): ?string
	{
		return $this->username;
	}

	public function setUsername(string $username): self
	{
		$this->username = $username;

		return $this;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getSurname(): ?string
	{
		return $this->surname;
	}

	public function setSurname(string $surname): self
	{
		$this->surname = $surname;

		return $this;
	}

	public function getEmailAddress(): ?string
	{
		return $this->emailAddress;
	}

	public function setEmailAddress(string $emailAddress): self
	{
		$this->emailAddress = $emailAddress;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getPassword(): ?string
	{
		return $this->password;
	}

	public function setPassword(string $password): self
	{
		$this->password = $password;

		return $this;
	}

	public function getPlainPassword(): ?string
	{
		return $this->plainPassword;
	}

	public function setPlainPassword(?string $plainPassword): self
	{
		$this->plainPassword = $plainPassword;

		return $this;
	}

	public function isActive(): ?bool
	{
		return $this->isActive;
	}

	public function setIsActive(bool $isActive): self
	{
		$this->isActive = $isActive;

		return $this;
	}

	public function getLastLoginAt(): ?\DateTimeInterface
	{
		return $this->lastLoginAt;
	}

	public function setLastLoginAt(?\DateTimeInterface $lastLoginAt): self
	{
		$this->lastLoginAt = $lastLoginAt;

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRoles()
	{
		return ['ROLE_OCTAVE_USER'];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getSalt()
	{
		return null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function eraseCredentials()
	{
		$this->plainPassword = null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isAccountNonExpired()
	{
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isAccountNonLocked()
	{
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isCredentialsNonExpired()
	{
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isEnabled()
	{
		return $this->isActive;
	}
}
