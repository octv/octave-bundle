<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Security controller for the Octave admin panel
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class SecurityController extends AbstractController
{
	/**
	 * Render the login view
	 *
	 * @param AuthenticationUtils $authenticationUtils
	 * 
	 * @Route("/login", name="octave_security_login")
	 */
	public function login(AuthenticationUtils $authenticationUtils): Response
	{
		// Get the login error if there is one
		$error = $authenticationUtils->getLastAuthenticationError();
		// Last username entered by the user
		$lastUsername = $authenticationUtils->getLastUsername();

		// Render login view
		return $this->render('@OctaveSecurity/login.html.twig', [
			'last_username' => $lastUsername,
			'error' => $error
		]);
	}

	/**
	 * Logout the user
	 * 
	 * @Route("/logout", name="octave_security_logout")
	 */
	public function logout()
	{
		// This should never be executed as Symfony handles the response
		throw new \Exception('Don\'t forget to activate logout in security.yaml');
	}
}