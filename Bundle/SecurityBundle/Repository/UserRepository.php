<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Octave\Bundle\SecurityBundle\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Security user repository
 *
 * @author Stan Jansen <stan@stanjan.nl>
 * 
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, User::class);
	}
}