<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * UsernameNotUniqueException is thrown if an username is already used by an User
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class UsernameNotUniqueException extends Exception
{
	/**
	 * @var string
	 */
	private $username;

	/**
	 * @param string $username
	 */
	public function __construct(string $username)
	{
		$this->username = $username;
		parent::__construct(sprintf('Username "%s" already exists', $username), 1101);
	}
}
