<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\SecurityBundle\EventListener;

use Octave\Bundle\SecurityBundle\Doctrine\UserManager;
use Octave\Bundle\SecurityBundle\Entity\User;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Fired when an User logs in
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class InteractiveLoginEventListener
{
	/**
	 * @var UserManager
	 */
	private $userManager;

	/**
	 * InteractiveLoginEventListener constructor
	 * 
	 * @param UserManager $userManager
	 */
	public function __construct(UserManager $userManager)
	{
		$this->userManager = $userManager;
	}

	/**
	 * Updates the logged in User's lastLoggedInAt property
	 * 
	 * @param InteractiveLoginEvent $event
	 */
	public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
	{
		// Get the User that logged in
		$user = $event->getAuthenticationToken()->getUser();

		// Make sure it's an User for the Octave context
		if (!($user instanceof User)) {
			return;
		}

		// Update last login
		$this->userManager->updateLastLoginAt($user);
	}
}
