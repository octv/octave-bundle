<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\CoreBundle\Tests\Unit\Menu;

use Octave\Bundle\CoreBundle\Menu\SidebarMenuBuilder;
use Octave\Bundle\MenuBundle\Model\Menu;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Unit tests for Octave\Bundle\CoreBundle\Menu\SidebarMenuBuilder
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class SidebarMenuBuilderTest extends WebTestCase
{
	/**
	 * Test if the menu builder can be built
	 */
	public function testBuild()
	{
		$builder = new SidebarMenuBuilder();
		$menu = $builder->build([]);
		$this->assertTrue($menu instanceof Menu);
	}
}