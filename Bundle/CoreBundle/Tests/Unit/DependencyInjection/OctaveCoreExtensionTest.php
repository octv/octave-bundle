<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\CoreBundle\Tests\Unit\DependencyInjection;

use Octave\Bundle\CoreBundle\Controller\DashboardController;
use Octave\Bundle\CoreBundle\DependencyInjection\OctaveCoreExtension;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Unit tests for Octave\Bundle\CoreBundle\DependencyInjection\OctaveCoreExtension
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class OctaveCoreExtensionTest extends WebTestCase
{
	/**
	 * Test if the extension loads the configuration properly
	 *
	 * @covers Octave\Bundle\CoreBundle\DependencyInjection\OctaveCoreExtension::load
	 */
	public function testLoad()
	{
		$extension = new OctaveCoreExtension();
		$container = new ContainerBuilder();
		$extension->load([], $container);

		// Test one of the services
		$this->assertTrue($container->hasDefinition(DashboardController::class));

		// Test one of the parameters
		$this->assertTrue($container->hasParameter('octave_route_prefix'));
	}
}