<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\CoreBundle\Tests\Functional\Controller;

use Octave\Bundle\CoreBundle\Controller\DashboardController;
use Octave\Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Functional tests for Octave\Bundle\CoreBundle\Controller\DashboardController
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class DashboardControllerTest extends TestCase
{
	/**
	 * Test if the index page is reachable with and without authentication
	 *
	 * @covers Octave\Bundle\CoreBundle\Controller\DashboardController::index
	 */
	public function testIndex()
	{
		$client = $this->getClient();

		// Test unauthenticated
		$client->request('GET', '/octave-admin/');
		$this->assertTrue($client->getResponse()->isRedirection());

		// Test authenticated
		$this->loginAsUser();
		$client->request('GET', '/octave-admin/');
		$this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
	}
}