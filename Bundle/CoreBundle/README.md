# Octave CMS / Core Bundle

## Config

Routing in `config/routes.yaml`:
```yaml
octave_core_bundle:
	resource: 	'@OctaveCoreBundle/Controller/'
	type:     	annotation
	prefix:   	'%octave_route_prefix%'
```

## Environment variables

* `OCTAVE_ROUTE_PREFIX`: Prefix for all CMS related routes, defaults to `/octave-admin`.

