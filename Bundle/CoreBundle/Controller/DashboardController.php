<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Dashboard controller for the Octave admin panel
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class DashboardController extends AbstractController
{
	/**
	 * Render the main dashboard view
	 *
	 * @return Response
	 * 
	 * @Route("/", name="octave_dashboard_index")
	 */
	public function index(): Response
	{
		// Render view
		return $this->render('@OctaveCore/dashboard/index.html.twig');
	}
}