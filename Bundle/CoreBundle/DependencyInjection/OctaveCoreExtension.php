<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\CoreBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Octave core extension for bundle configuration loading
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class OctaveCoreExtension extends Extension
{
	public function load(array $configs, ContainerBuilder $container)
	{
		// Setup the YAML loader for the config Resources/config folder
		$loader = new YamlFileLoader(
			$container,
			new FileLocator(__DIR__.'/../Resources/config')
		);

		// Load the services
		$loader->load('services.yaml');
	}
}