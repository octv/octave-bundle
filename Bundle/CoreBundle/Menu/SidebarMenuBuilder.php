<?php

/*
 * This file is part of Octave
 *
 * (c) Stan Jansen <stan@stanjan.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Octave\Bundle\CoreBundle\Menu;

use Octave\Bundle\MenuBundle\Builder\AbstractBuilder;
use Octave\Bundle\MenuBundle\Model\MenuInterface;

/**
 * Sidebar menu builder
 *
 * @author Stan Jansen <stan@stanjan.nl>
 */
class SidebarMenuBuilder extends AbstractBuilder
{
	/**
	 * {@inheritdoc}
	 */
	protected function initialize(MenuInterface $menu, array $attributes)
	{

	}
}