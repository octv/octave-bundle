//
// Octave logo
//

(function() {
	var octaveLogo = document.querySelector('.octave-logo');

	if (octaveLogo) {
		var icon = octaveLogo.querySelector('.icon');
		var bottomCircle = icon.querySelector('.bottom-circle');

		// Rotate the bottom circle of the logo's icon towards the icon
		octaveLogo.addEventListener('mouseover', (event) => {
			var boundingClient = icon.getBoundingClientRect();
			var centerX = boundingClient.left + boundingClient.width / 2;
			var centerY = boundingClient.top + boundingClient.height / 2;
			var radians = Math.atan2(event.y - centerY, event.x- centerX);
			var degrees = radians * (180 / Math.PI);
			bottomCircle.style.transform = 'rotate(' + degrees + 'deg)';
		});

		// Reset the bottom circle of the logo's icon rotation
		octaveLogo.addEventListener('mouseout', () => {
			bottomCircle.style.transform = 'rotate(0deg)';
		});
	}
})();