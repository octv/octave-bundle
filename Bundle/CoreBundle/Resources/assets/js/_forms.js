//
// Forms
//

(function() {
	//
	// Toggle the "has-input", "has-focus" and "has-error" classes on input groups 
	//
	var groupInputs = document.querySelectorAll('.input-group > input');

	groupInputs.forEach(input => {
		var inputGroup = input.parentElement;

		updateInputStatus(inputGroup, input);

		input.addEventListener('focus', () => {
			inputGroup.classList.add('has-focus');
		});
		input.addEventListener('blur', () => {
			inputGroup.classList.remove('has-focus');
		});
		input.addEventListener('invalid', () => {
			inputGroup.classList.add('has-error');
		});
		input.addEventListener('input', () => {
			updateInputStatus(inputGroup, input);

			if (input.checkValidity()) {
				inputGroup.classList.remove('has-error');
			} else {
				inputGroup.classList.add('has-error');
			}
		});
	});

	function updateInputStatus(inputGroup, input)
	{
		if (input.value == '' || input.value ==null) {
			inputGroup.classList.remove('has-input');
		} else {
			inputGroup.classList.add('has-input');
		}
	}

	//
	// Add the "loading" class to all submit buttons and the octave logo whenever a form gets submitted
	//
	var forms = document.querySelectorAll('form');

	forms.forEach(form => {
		form.addEventListener('submit', () => {
			var submitButtons = form.querySelectorAll('input[type="submit"], button[type="submit"]');

			submitButtons.forEach(button => {
				button.classList.add('loading');
			});

			var octaveLogo = document.querySelector('.octave-logo');
			if (octaveLogo) {
				octaveLogo.classList.add('loading');
			}
		});		
	});
})();