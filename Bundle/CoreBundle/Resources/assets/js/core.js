/*!
 * Octave Core
 * Copyright 2019 Stan Jansen
 * Licensed under MIT (https://gitlab.com/octv/octave-bundle/blob/master/LICENSE)
 */

require('./_forms');
require('./_logo');